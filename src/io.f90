module io

! A collection of useful functions and subroutines for input and output

!------------------------------------------------------------------------------

  use consts

  implicit none

  private
  public CheckReadable, ParseCommandLine

  contains

!------------------------------------------------------------------------------

subroutine Usage()

  ! Output usage instructions for the code.
  character(len=:), allocatable :: descr_text

  descr_text = "This is a description of how to use the code. " //  &
      "We can wrap this over multiple lines. The sample executable " // &
      "expects a single argument that is a filename. It doesn't do" // &
      "anything except check the file is readable, and then allocate " // &
      "an array."

  write(stdout, '(/, A)') "  USAGE:"
  call OutputFixedWidth(4, 80,  descr_text)

end subroutine Usage

!------------------------------------------------------------------------------

subroutine ParseCommandLine(input_file)

  ! Parse any input flags and the input file name from the command line.

  ! Out:
  !   input_file - the name of the file to operate on.

  character(len=:), allocatable, intent(out) :: input_file

  integer :: iarg, narg
  character(len=max_string_size) :: argc

  narg = command_argument_count()

  ! The following is excessive for reading 1 argument, but is a useful
  ! starting point in case more functionality is required.

  if (narg < 1) then
    write(stderr, *) "Error: At least one argument is required."
    call Usage()
    error stop exec_error_code
  elseif (narg > 1) then
    write(stderr, *) "Error: Too many arguments."
    call Usage()
    error stop exec_error_code
  else
    ! Loop over args
    do iarg = 1, narg
      call get_command_argument(iarg, argc)
      select case(adjustl(argc))
      ! We support a -h flag here. This can be extended as needed.
      case("-h", "--help")
        call Usage()
        stop
      case default
        input_file = trim(argc)
      end select
    enddo
  endif

end subroutine ParseCommandLine

!------------------------------------------------------------------------------

subroutine CheckReadable(filename)

  ! Check that a file exists and can be opened

  character(len=*) :: filename

  integer :: ios, myunit
  logical :: exists

  inquire(file=filename, exist=exists)
  if (.not. exists) then
    write(stderr, *) trim(filename), ' not found. Exiting.'
    error stop file_error_code
  endif

  open(newunit=myunit, file=filename, iostat=ios)
  if (ios /= 0) then
    write(stderr, *) trim(filename), ' cannot be read. Exiting.'
    error stop file_error_code
  endif
  close(myunit)

end subroutine CheckReadable

!------------------------------------------------------------------------------

subroutine OutputFixedWidth(n_indent, linewidth, string, o_unit_in)

  ! Write out a string to o_unit, indenting it the requested number of chars
  ! and wrapping it sensibly as close as we can to linewidth characters wide.

  ! Input:
  !  n_indent - the number of spaces to indent this line.
  !  linewidth - the width at which to wrap text.
  !  outstring - the text to be output
  !  o_unit_in -  the output unit to write to. (optional). stdout by default.

  integer, intent(in) :: n_indent, linewidth
  character(len=*), intent(in) :: string
  integer, intent(in), optional :: o_unit_in

  integer :: o_unit, pos1, pos2
  character(len=linewidth) :: outline
  character(len=:), allocatable :: stringl

  if (present(o_unit_in)) then
    o_unit = o_unit_in
  else
    o_unit = stdout
  endif

  ! Get rid of any leading space
  stringl = trim(string)
  stringl = adjustl(stringl)

  ! Find the last space before position (linewidth - n_indent) and write out
  ! till this point (if the remaining output is greater than available).
  pos1 = 1
  pos2 = 1
  do
    write(outline, '(A)') " "

    ! Note we're actually breaking at character (linewidth - 1) here. Usually
    ! this looks a little better.
    if (len_trim(stringl(pos1:)) + n_indent < linewidth) then
      outline(n_indent + 1:) = stringl(pos1:)
      write(o_unit, '(A)') trim(outline)
      exit
    else
      pos2 = pos1 + index(trim(stringl(pos1: pos1 + linewidth - n_indent - &
          1)), ' ', back=.true.) - 1
      outline(n_indent + 1:) = stringl(pos1: pos2 - 1)
      write(o_unit, '(A)') trim(outline)
      pos1 = pos2 + 1 ! skip the space character
    endif
  enddo

end subroutine OutputFixedWidth

!------------------------------------------------------------------------------

end module io
